import argparse
import logging as log
import cPickle as pickle
import math
import numpy as np
from scipy.optimize import fmin_l_bfgs_b
import sys

from rae_train import RAETrain


def main():
    ALPHA = 0.2
    NUM_OF_CLASSES = 2
    VECTOR_SIZE = 20

    # Setup command line argument parsing
    parser = argparse.ArgumentParser(
        description="Recursive Autoencoder Sentiment Analysis",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('-v', help="increase verbosity of output",
                        dest='verbose', action='store_true')
    parser.add_argument('--train',
                        help='Train an RAE network',
                        action='store_true')
    parser.add_argument('--diff',
                        help='test gradient using numerical methods',
                        action='store_true')
    parser.add_argument('--test',
                        help='test data using a model')
    parser.add_argument('--pickle_in',
                        help='input dataset pickle file')
    parser.add_argument('--pickle_out',
                        help='output model pickle file name',
                        dest='pickle_out')

    log.basicConfig(level=log.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='lda.log',
                    filemode='w')
    args = parser.parse_args()

    # Setup debugging output
    if args.verbose:
        console = log.StreamHandler()
        console.setLevel(log.INFO)
        formatter = log.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
        console.setFormatter(formatter)
        log.getLogger('').addHandler(console)

    # Load the dataset and initialize Parameter matrices
    if args.pickle_in:
        print '[*] Loading pickle: {0}'.format(args.pickle_in)
        vocab_dict, data = pickle.load(open(args.pickle_in, 'rb'))
        # Initialize Embedding Matrix from uniform dist. with interval [-r, r]
        # This is from the Socher Matlab code.
        r = math.sqrt(6) / math.sqrt(2 * VECTOR_SIZE + 1)
        L = 0.001 * (np.random.rand(VECTOR_SIZE,
                                   len(vocab_dict)) * 2 * r - r)

    else:
        print "[!] Must give an input file"
        sys.exit(1)

    # Test an existing model
    if args.test:
        print '[*] TESTING is NOT implemented yet!'
        training_wrapper = RAETrain(data, L, vocab_dict,
                                    VECTOR_SIZE, NUM_OF_CLASSES, ALPHA)

        theta = pickle.load(open(args.test, 'rb'))

        accuracy = training_wrapper.validate(theta)
        print '[*] Accuracy = {0}'.format(accuracy)
    # Train a new model
    elif args.train:
        print '[*] TRAINING'

        training_wrapper = RAETrain(data, L, vocab_dict,
                                    VECTOR_SIZE, NUM_OF_CLASSES, ALPHA)

        x, f, d = fmin_l_bfgs_b(training_wrapper.get_total_error_and_gradient,
                                np.random.rand(1700) * 2 * r - r, maxfun=22)
        print x
        print f
        print d

        accuracy = training_wrapper.validate(x)
        print '[*] Accuracy = {0}'.format(accuracy)
        pickle.dump(x, open('model.pickle', 'w'))
    # Compute the gradient numerically and test with gradient function
    elif args.diff:
        print '[*] Testing the Gradient Function'
        training_wrapper = RAETrain(data, L, vocab_dict,
                                    VECTOR_SIZE, NUM_OF_CLASSES, ALPHA)

        training_wrapper.numerical_diff_test()

if __name__ == '__main__':
    main()
