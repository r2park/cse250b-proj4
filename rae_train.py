import numpy as np

from numpy import linalg as LA
from btree import Btree
from sklearn import linear_model


class RAETrain:
    def __init__(self, dataset, L, vocab_dict,
                 vector_size=20, num_of_classes=2, alpha=0.2):
        self.data = dataset
        self.VECTOR_SIZE = vector_size
        self.ALPHA = alpha
        self.NUM_OF_CLASSES = num_of_classes
        self.L = L
        self.vocab_dict = vocab_dict

        self.tdata, self.vdata = self._partition_data()

    def get_total_error_and_gradient(self, theta):
        count = 0
        total_partials = np.zeros(1700)
        total_error = 0
        for label, sentence in self.tdata:
            if len(sentence) < 2:
                count += 1
                continue

            smv = []
            for word in sentence:
                smv.append(self.L[:, self.vocab_dict[word]])

            btree = Btree(sentence,
                          smv,
                          np.array([0, 1]) if label else np.array([1, 0]))

            error, partials = btree.get_err_grad(theta)

            total_error += error #+ 0.5 * partials.dot(partials)
            total_partials += partials
            #btree.pp()

        return total_error / (len(self.tdata) - count), total_partials / (len(self.tdata) - count)

    def validate(self, theta):
        count = 0
        total_examples = 0.0
        correct_examples = 0.0
        for label, sentence in self.vdata[:100]:
            if len(sentence) < 2:
                count += 1
                continue

            smv = []
            for word in sentence:
                smv.append(self.L[:, self.vocab_dict[word]])

            btree = Btree(sentence,
                          smv,
                          np.zeros(self.NUM_OF_CLASSES))

            pred = btree.predict(theta)

            if np.argmax(pred) == label:
                correct_examples += 1.0

            total_examples += 1.0

        return correct_examples / total_examples

    def numerical_diff_test(self):
        epsilon = 0.1
        r = np.sqrt(6) / np.sqrt(2 * 20 + 1)
        theta = np.random.rand(1700) * 2 * r - r

        for _ in xrange(15):
            num_list = []

            label, sentence = self.tdata[0]
            smv = []

            for word in sentence:
                smv.append(self.L[:, self.vocab_dict[word]])

            btree = Btree(sentence,
                            smv,
                            np.array([0, 1]) if label else np.array([1, 0]))

            _, partials = btree.get_err_grad(theta)
            btree.pp()
            for w_ij in range(1700):
                theta_prime = theta[:]
                theta_prime[w_ij] += epsilon
                error1 = btree.get_err(theta_prime)

                theta_prime = theta[:]
                theta_prime[w_ij] -= epsilon
                error2 = btree.get_err(theta_prime)

                numerical_result = (error1 - error2) / (2 * epsilon)
                num_list.append(numerical_result)

            smv[:] = []

            assert len(partials) == len(num_list)
            with open('signs.out', 'w') as fs, open('partials.out', 'w') as f, open('difftest.out', 'a') as f2:
                for numeric, partial in zip(num_list, partials):
                    f.write('{0} : {1}\n'.format(numeric, partial))
                    fs.write('{0}\n'.format('0' if numeric*partial >= 0 else 'X'))

                diff_list = num_list - partials
                l2norm = LA.norm(diff_list, ord=2)
                f2.write('{0}, {1}\n'.format(epsilon, l2norm))

            print '[*] Epsilon = {1} L2 = {0}'.format(l2norm, epsilon)
            epsilon /= 10

    def _partition_data(self):
        rdata = self.data
        m = len(rdata)
        np.random.shuffle(rdata)
        tm = int(m * 0.9)
        tdata = rdata[:tm]
        vdata = rdata[tm:]
        return tdata, vdata
