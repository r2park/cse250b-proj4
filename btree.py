import Queue
import sys

import numpy as np
import pydot
from autoencoder import Autoencoder


class Btree:
    def __init__(self, sentence, word_vectors, t, vector_size = 20, num_of_classes = 2, alpha=0.2):
        self.VECTOR_SIZE = vector_size
        self.NUM_OF_CLASSES = num_of_classes
        self.sentence = sentence
        self.wv = word_vectors
        self.nodes = []
        self.W1 = None
        self.B1 = None
        self.W2 = None
        self.B2 = None
        self.V = None
        self.t = t
        self.alpha = 0.2

        self.Troot = None

    class LeafNode:
        def __init__(self, word, word_vector, V, t, alpha=0.2):
            self.word = word
            self.x = word_vector
            self.isLeaf = True
            self.V = V
            self.t = t
            self.alpha = alpha
            self.p = self.get_predicted_label()
            if t.any():
                self.d_l = self.get_d_l()

        def get_N(self):
            return 0.0

        def get_predicted_label(self):
            return self.softmax(self.V.dot(self.x))

        def get_d_l(self):
            """ computes delta for the label output node """
            return (1 - self.alpha)*(self.p - self.t)

        def softmax(self, w):
            e = np.exp(w)
            dist = e / np.sum(e)

            return dist

        def get_node_error(self):
            #return -1 * self.t.dot(np.log(self.p))
            return 0

    def _initialize_leaf_nodes(self):
        self.nodes[:] = []
        for word, meaning in zip(self.sentence, self.wv):
            if word == ',':
                word = 'COMMA'
            self.nodes.append(self.LeafNode(word, meaning, self.V, self.t))

    def get_err(self, theta):
        self.W1 = theta[:800].reshape(self.VECTOR_SIZE, 2 * self.VECTOR_SIZE)
        self.B1 = theta[800:820].reshape(self.VECTOR_SIZE, 1)
        self.W2 = theta[820:1620].reshape(2 * self.VECTOR_SIZE,
                                          self.VECTOR_SIZE)
        self.B2 = theta[1620:1660].reshape(2 * self.VECTOR_SIZE, 1)
        self.V = theta[1660:1700].reshape(self.NUM_OF_CLASSES,
                                          self.VECTOR_SIZE)
        self._initialize_leaf_nodes()
        self.Troot = self._build_tree()

        return self.Troot.get_total_error()

    def get_err_grad(self, theta):
        self.W1 = theta[:800].reshape(self.VECTOR_SIZE, 2 * self.VECTOR_SIZE)
        self.B1 = theta[800:820].reshape(self.VECTOR_SIZE, 1)
        self.W2 = theta[820:1620].reshape(2 * self.VECTOR_SIZE,
                                          self.VECTOR_SIZE)
        self.B2 = theta[1620:1660].reshape(2 * self.VECTOR_SIZE, 1)
        self.V = theta[1660:1700].reshape(self.NUM_OF_CLASSES,
                                          self.VECTOR_SIZE)
        self._initialize_leaf_nodes()
        self.Troot = self._build_tree()


        d_root = (self.Troot.x_prime * (self.W2.T.dot(self.Troot.d_r))
                                        + self.V.T.dot(self.Troot.d_l)).reshape(20, 1)
        #d_root = np.zeros((20,1))

        W1_p = d_root.dot(np.concatenate((self.Troot.c1, self.Troot.c2)).reshape(40,1).T)
        B1_p = d_root[:]
        #W1_p = np.zeros(800)
        #B1_p = np.zeros(20)
        W2_p = self.Troot.d_r[:, None].dot(self.Troot.x.reshape(20,1).T)
        B2_p = self.Troot.d_r[:]
        V_p = self.Troot.d_l[:, None].dot(self.Troot.x.reshape(20,1).T)

        partial = np.concatenate((W1_p.flatten(),
                                  B1_p.flatten(),
                                  W2_p.flatten(),
                                  B2_p.flatten(),
                                  V_p.flatten()
                                  ))

        gradient = 2/(self.Troot.n1 + self.Troot.n2) * (partial+(self._get_gradient(self.Troot,
                                                                            d_root,
                                                                            self.Troot.z1,
                                                                            self.Troot.x_prime,
                                                                            True, True) +
                                                            self._get_gradient(self.Troot,
                                                                            d_root,
                                                                            self.Troot.z2,
                                                                            self.Troot.x_prime,
                                                                            False, True)))

        return self.Troot.get_total_error(), gradient

    def _get_gradient(self, root, parent_d, parent_z, parent_x_prime, isLeft, isRoot):
        #recon_del = -0.2 * (root.x - parent_z)
        recon_del = 0

        if root.isLeaf:
            #d_j = (self.W1[:, :20].T.dot(parent_d)[:,0] + recon_del).reshape(20,1)
            #W1_p = d_j.dot(np.concatenate((root.c1, root.c2)).reshape(40,1).T)
            #B1_p = d_j[:]
            #W2_p = d_j.dot(root.x.reshape(20,1).T)
            #B2_p = d_j
            #V_p = d_j.dot(root.x.reshape(20,1).T)
            #partial = np.concatenate((W1_p.flatten(),
                                    #B1_p.flatten(),
                                    #np.zeros(1080)
                                    #))
            partial = np.zeros(1700)
            return partial

        W1_half = self.W1[:, :20] if isLeft else self.W1[:, 20:]

        d_j = (root.x_prime * (W1_half.T.dot(parent_d)[:, 0] +
                            self.W2.T.dot(root.d_r) +
                            self.V.T.dot(root.d_l) +
                            recon_del)).reshape(20,1)

        W1_p = d_j.dot(np.concatenate((root.c1, root.c2)).reshape(40, 1).T)
        B1_p = d_j[:]
        W2_p = root.d_r[:, None].dot(root.x.reshape(20,1).T)
        B2_p = root.d_r[:]
        V_p = root.d_l[:, None].dot(root.x.reshape(20,1).T)

        partial = np.concatenate((W1_p.flatten(),
                                  B1_p.flatten(),
                                  W2_p.flatten(),
                                  B2_p.flatten(),
                                  V_p.flatten()
                                  ))

        return partial + (self._get_gradient(root.left,
                                                 d_j,
                                                 root.z1,
                                                 root.x_prime,
                                                 True, False) +
                              self._get_gradient(root.right,
                                                 d_j,
                                                 root.z2,
                                                 root.x_prime,
                                                 False, False))

    def _build_tree(self):
        while len(self.nodes) > 1:
            minErr = sys.maxint
            newNode = None

            j = 0
            for i in range(len(self.nodes)-1):
                tempNode = Autoencoder(self.W1,
                                       self.B1,
                                       self.W2,
                                       self.B2,
                                       self.V,
                                       self.nodes[i].x,
                                       self.nodes[i + 1].x,
                                       self.nodes[i].get_N() + 1.0,
                                       self.nodes[i + 1].get_N() + 1.0,
                                       self.t,
                                       self.nodes[i],
                                       self.nodes[i + 1])
                if tempNode.e1 < minErr:
                    minErr = tempNode.e1
                    newNode = tempNode
                    j = i
            self.nodes[j] = newNode
            self.nodes.remove(newNode.right)

        return newNode

    def predict(self, theta):
        self.W1 = theta[:800].reshape(self.VECTOR_SIZE, 2 * self.VECTOR_SIZE)
        self.B1 = theta[800:820].reshape(self.VECTOR_SIZE, 1)
        self.W2 = theta[820:1620].reshape(2 * self.VECTOR_SIZE,
                                          self.VECTOR_SIZE)
        self.B2 = theta[1620:1660].reshape(2 * self.VECTOR_SIZE, 1)
        self.V = theta[1660:1700].reshape(self.NUM_OF_CLASSES,
                                          self.VECTOR_SIZE)
        self._initialize_leaf_nodes()
        self.Troot = self._build_tree()

        return self.Troot.p

    def pp(self):
        graph = pydot.Dot(graph_type='graph', ordering='out')

        q = Queue.Queue()

        q.put(self.Troot)

        while not q.empty():
            node = q.get()
            if not node.isLeaf:
                if node.left.isLeaf:
                    graph.add_edge(pydot.Edge(id(node), node.left.word))
                else:
                    graph.add_edge(pydot.Edge(id(node), id(node.left)))

                if node.right.isLeaf:
                    graph.add_edge(pydot.Edge(id(node), node.right.word))
                else:
                    graph.add_edge(pydot.Edge(id(node), id(node.right)))

                q.put(node.left)
                q.put(node.right)

        graph.write_png('graph.png')
