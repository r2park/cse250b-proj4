\documentclass[a4paper,11pt]{article}
\usepackage{color}        % for colored output
\usepackage[parfill]{parskip}      % Activate to begin paragraphs with an empty line rather than an indent
\usepackage[top=1in, bottom=1in, left=.7in, right=.7in]{geometry}  % document format
\usepackage{wrapfig}
\usepackage{url}
\usepackage{tikz}
\usepackage{tikz-qtree}
\usepackage{graphicx}                % graphics, graphs, pictures
\usepackage{soul}        % underlines text nicely (command: \ul}
\usepackage{amsmath}               % for advanced mathematics
\usepackage{amssymb}
\usepackage{bm}                          % bold math symbols (command: \bm)
\usepackage{ctable}                    % Include footnotes in the table
\usepackage{longtable}     % splits long table on multiple pages
\usepackage{rotating}
\usepackage{tabularx}
\usepackage{lscape}                   % landscape
\usepackage{fancyhdr}
\pagestyle{fancy}
\usepackage{lscape}
\usepackage{helvet}
\usepackage{float}
\floatstyle{plaintop}
\usepackage[utf8]{inputenc} 



\title{\textbf{Semi-Supervised Recursive Autoencoders for Predicting Sentiment Distributions}}
\author{
        Richard Park\\
              r2park@acsmail.ucsd.edu\\
        \and
        Mofei Li\\
               mol017@ucsd.edu\\
}
\date{March 16, 2014}


\begin{document}

\maketitle

\begin{abstract}
In this project, we implement the Semi-Supervised Recursive Autoencoder(RAE) with random word initialization to learn sentiment labels of sentences on the movie reviews dataset. The accuracy for our model is 57\%, which is not comparable to the result of 76.8\% reported Socher et. al\cite{Socher}.\\
\end{abstract}

\section{Introduction}
Recursive Autoencoder is a powerful tool to learn the sentiments in data such as personal experiences, products reviews, movie reviews etc. This method can capture more complex linguistic phenomena comparing with its bag-of-words counterpart\cite{Socher}. Staring with assigning a high-dimensional random vector to each word in the sentence, the goal of autoencoders is to learn the representation of the entire sentence. 
\section{Recursive Autoencoder}
\subsection{Recursive Definition of Meaning of a Sentence}

For each word in the lexicon, we initialize its meaning $x_i$ as a random vector in $\mathbb{R}^n$. $x_i$'s are i.i.d with $x_i\sim  \mathcal{N}(0,\sigma^2 \mathbf{I}_n)$. The meaning of a phrase $p$, given its two children $c_1$ and $c_2$ in a binary tree structure, is defined as a function of the meanings of its children:
$$p = f\left(W^{(1)}[c_1;c_2] + b^{(1)}\right)$$
where $W^{(1)} \in {\mathbb{R}}^{n \times 2n}$ by the concatenation of two children, $b^{(1)} \in {\mathbb{R}}^n$, and $f(\cdot)$ is an element-wise sigmoid-shaped activation function from ${\mathbb{R}}^n$ to interval $[-1,+1]^n$, in this assignment we use the hyperbolic tangent function $\tanh(\cdot)$.
The predicted meaning vector of a whole sentence is $x_r$, where $r$ is the root node. This is called compositional approach to semantics.

\subsection{Recursive Autoencoders}
For each node, the autoencoder we use has three layers:
\begin{itemize}
\item An input layer:$c_1$ and $c_2$, which are children of a internal node $p$
\item A sigmoid hidden layer:
$$p = f\left(W^{(1)}[c_1:c_2] + b^{(1)}\right)$$
\item An output layer(or reconstruction layer): 
$$[c_1';c_2'] = W^{(2)}p + b^{(2)} $$
where $W^{(2)} \in {\mathbb{R}}^{n \times 2n}$ and $b^{(2)} \in {\mathbb{R}}^n$.
\end{itemize}
Using autoencoder, we try to reconstruct the children of an internal node to assess how well a $n$-dimensional vector of a internal node represents the meaning of its children.\\  
During the training, our goal is to minimize the reconstruction errors of this input pair. The weighted reconstruction error of internal node $p$ is defined as
\begin{align*}
E_{rec}([c_1;c_2]) &= \frac{1}{2} {\| [c_1;c_2] - [c_1';c_2'] \|}^2 \\
&= \frac{n_1}{n_1 + n_2} \|c_1 - c_1' \| ^2 + \frac{n_2}{n_1 + n_2} \|c_2 - c_2' \| ^2
\end{align*}
where $n_1, n_2$ be the number of words underneath a current potent child.

\subsection{Greedy Unsupervised RAE}
To build up the binary tree structure, we apply autoencoder recursively. For a sentence with $m$ words, firstly compute the reconstruction error $(E_{rec})$for $m-1$ pairs of consecutive words. Next, it selects the pair with the lowest $E_{rec}$ and replace them with a parent node $p$, which represents the phrase consisted of its children words. Treat the new node $p$ like any other input nodes and the process repeats until all the input nodes collapse into one node $r$. 

\begin{figure}[ht!]
\centering
\textit{"it's so laddish and juvenile , only teenage boys could possibly find it funny."}
\tikzset{every tree node/.style={align=center,anchor=north}}
\Tree [.$\bigcirc$ [.$\bigcirc$ [.it ] 
                                [.$\bigcirc$ [.$\bigcirc$ [.$\bigcirc$ 's so ] 
                                             [.laddish ] ]  
                                [.$\bigcirc$ [.$\bigcirc$ and juvenile ]
                                             [., ] ] ] ] 
                   [.$\bigcirc$ [.$\bigcirc$ only teenage ]
                                   [.$\bigcirc$ [.$\bigcirc$ [.$\bigcirc$ boys could ] 
                                                [.possibly ] ]
                                   [.$\bigcirc$ [.$\bigcirc$ find it ]
                                                [.$\bigcirc$ funny . ] ] ] ] ] 

\caption{The binary tree structure of a sentence labeled as negative movie review dataset}
\label{Figure1}
\end{figure}


\subsection{Semi-Supervised RAE for Labels Predicting}
The unsupervised RAE we have completed so far capture the semantics of multi-word phrases(meaning vectors for internal nodes in the binary tree) or sentence(meaning vector for the root node in the binary tree). We can add a log-linear model on top of the meaning vectors to get phrase-level or sentence-level prediction of sentiment label distribution, which means adding a $\mathsf{softmax}$ layer on top of each parent node:
$$d(p; \theta) = \mathsf{softmax}(W^{(lb)} p)$$
Assume there are $K$ labels, $d \in {\mathbb{R}}^K$ is a $K$-dimensional multinomial distribution. If $K = 2$, then the linear model is a standard logistic regression classifier, which is the case in the dataset of this assignment.\\
The cross-entropy error is 
$$E_{cE}(p,t;\theta) = -\sum_{k=1}^{K}{ \log{d_k(p;\theta)}}$$

\subsection{Learning}
The final semi-supervised RAE objective is to minimize both the reconstruction error and the cross-entropy error across the all nodes of the entire tree. The objective function over (sentences, label) pairs $(x,t)$ in a corpus becomes
\begin{align*}
J &= \frac{1}{N}\sum_{(x,t)}E(x,t;\theta) + \frac{\lambda}{2}\|\theta\|^2 \\
&= \frac{1}{N}\sum_{(x,t)}\sum_{s \in T(RAE_{\theta}(x))}{E\left([c_1;c_2]_s,p_s,t,\theta\right)} + \frac{\lambda}{2}\|\theta\|^2 \\
&= \frac{1}{N}\sum_{(x,t)}\sum_{s \in T(RAE_{\theta}(x))}\left[{\alpha E_{rec}([c_1;c_2]_s;\theta) + (1-\alpha)E_{cE}(p_s,t;\theta)}\right] + \frac{\lambda}{2}\|\theta\|^2
\end{align*}

where $\theta = \left(W^{(1)}, b^{(1)}, W^{(2)}, b^{(2), L} , W^{(lb)}, b^{(lb)}\right)$ is the set of parameters in our model, $s \in T(RAE_{\theta}(x))$ indicates we have an error for each entry in the training set that is the sum over the error at the nodes of the tree that is constructed by the greedy RAE, and the hyperparameter $\alpha$ weighs reconstruction error and the cross-entropy error.\\
We compute the gradients of $J$ with respect to $\theta$'s with
$$\frac{{\partial J}}{\partial \theta} = \frac{1}{N}\sum_{(x,t)}\frac{\partial E \left(x,t,;\theta \right)}{\partial \theta} + \lambda \theta$$ 
and use L-BFGS quasi-Newton method to to optimize $\theta$ based on the whole training set and therefore minimize $J$.\\
With backpropagation method, we can compute the derivatives efficiently.

For denotation simplicity, let $W^{(1)}_* = [W^{(1)},b^{(1)}]$, $W^{(2)}_* = [W^{(2)},b^{(2)}]$ and $W^{(lb)}_* = [W^{(lb)},b^{(lb)}]$. $W^{(1)}_*$, $W^{(2)}_*$ and $W^{(lb)}_*$ here are extended matrices.\\
Let $p \in \mathbb{R}^n$ be the feedforward column vector value of a node, whose value is given by
$$p = f(a) = f(W^{(1)}_*[c_1;c_2;1])$$
And the reconstructed node can be expressed as
$$[c_1';c_2'] = f(e) = f(W^{(2)}_*[p;1])$$

\begin{equation} \label{eq:W1}
\frac{\partial J}{\partial W^{(1)}_{*ij}} = {\frac{\partial J}{\partial a_i}} {\frac{\partial a_i}{\partial W^{(1)}_{*ij}}} := \delta_i {\frac{\partial a_i}{\partial W^{(1)}_{*ij}}}
\end{equation}
since the scalar $W^{(1)}_{*ij}$ influences $J$ only through the $i$th element $a_i$ of $a$. The factor on the far right of the equation \ref{eq:W1} in vector notation is
$$\frac{\partial a_i}{\partial W^{(1)}_{*i:}} = [c_1;c_2;1]^T$$
For each output node, assume the contribution to $J$ from node $p$ is the sum over the elements of $p$, then
$$\delta_i = \frac{\partial J}{\partial a_i} = \frac{\partial \sum_k L_k}{\partial a_i} = \frac{\partial L_i}{\partial a_i} = \frac{\partial L_i}{\partial p_i} \frac{\partial p_i}{\partial a_i} = \frac{\partial L_i}{\partial p_i} f'(a_i)$$
In our case, if the output node in the reconstruction layer,
$$\delta_i = \alpha (p_i -t_i) sech^2(a_i)$$
If the output node lies in the label layer,  
$$\delta_i = (1-\alpha)\frac{t_i}{p_i} sech^2(a_i)$$
For a non-output node,

$$\delta = f'(a)\circ\left[\sum_{k}(\delta^k)^T V^k\right]^T$$
where
where $V^k$ is the portion of the weighted matrix $W^{(1)_*}$. \\
For each $\delta_i$ in the $\delta$ matrix,

\begin{align*}
\delta_i &= f'(a_i)\sum_{k}(\delta^k)^TV^k_{:i}\\
&= f'(a_i)\left(W^{(1)T}\delta_p + W^{(2)T}\delta_{rec} + W^{(lb)}\delta_{lb}\right)
\end{align*}
where $\delta_p$ is the $\delta$ from parent node, in the special case of root node, which doesn't have a parent $\delta_p = 0$, $\delta_{rec}$ is the reconstruction nodes related to node $i$, and $\delta_{lb}$ is the label node related to node $i$.

\section{Dataset}
The dataset used for this experiment consists of  movie reviews from the 
Rotten Tomatoes website. This is the same data used by Socher et. al \cite{Socher}.
The dataset contains 10662 sentences which are labeled as expressing either
a positive or negative sentiment. We randomly shuffle the sentence ordering and
perform a 10/90 split on the dataset into a test and training set. We also separate
 the training set for training and validation using a 75/25 split in order to evaluate our models.
\section{Implementation}
We implement a semi-supervised Recursive Autoencoder (RAE) in the Python programming language.
\subsection{Sentence Structure}
The syntactic structure of each sentence is represented as a binary tree where each leaf node
is a word in the sentence and the internal nodes are autoencoders.
An Autoencoder class encapsulates each internal node i.e. a single
instance of an Autoencoder represents a single internal node's inputs, outputs, and state. 
The parent child relation between instances of autoencoders is maintained by a binary tree
which is created using the greedy algorithm described by Elkan \cite{Elkan} and Altwaijry et. al \cite{Altwaijry}.
\subsection{Feed-Forward}
The reconstruction and predicted label outputs of each internal node are calculated during the creation of the tree
structure. The corresponding error and partial derivative values for each output are calculated as each new internal node is added
to the tree.
\subsection{Backpropagation}
In order to calculate the gradient of the loss function we recursively propagate $\delta$ to child nodes starting at the root of the tree.
At each node $k$, $\frac{\partial J_k}{\partial \theta}$ is calculated and the the full partial derivative for $J$ is computed by summing
all partial derivatives $J_k$.

We verify the gradient $\frac{\partial J}{\partial \theta}$ by numerically computing the derivatives with central differences where
$$\frac{\partial J}{\partial \theta} = \frac{J(w_{ij}+\epsilon) - J(w_{ij} - \epsilon)}{2\epsilon}$$
We compare the derivatives output by the numerical method and backpropagation by calculating the L2-norm of their difference. Fig. \ref{Fig:difftest} plots 
the $L2$-norm vs increasingly smaller values of epsilon on a $log$-$log$ plot. The results show that the smallest value of $\epsilon$ that can be used is $10^{-13}$, smaller
values lead to greater divergence between the numerical and backpropagation derivatives.

\begin{figure}[ht!]
\centering
\includegraphics[scale=0.5]{difftest.jpeg}
\caption{$log$-$log$ plot of the $L2$-norm comparison between numerically derived gradient and the gradient calculated by backpropagation}
\label{Fig:difftest}
\end{figure}

\section{Experiment}
We test a model that was trained with an implementation that keeps the word meanings fixed.
We were able to verify that the backpropagation was correctly calculating derivatives for $J$ with respect to $W^{(2)}$, $B^{(2)}$,and  $W^{(lb)}$.
However, we had difficulty with calculating the correct backpropagation derivatives for $J$ with respect to $W^{(1)}$ and $B^{(1)}$. Our initial
models achieved poor performance that was no better than randomly choosing a sentiment. It was only after we improved the backpropagation derivatives with
respect to $W^{(1)}$ and $B^{(1)}$ that we began to see an improvement in accuracy.
The final model that we were able to train without regularization achieves accuracy of 57\% on the movie reviews dataset.

\section{Lessons Learned}
We attempted to implement the algorithm using only the class notes \cite{Elkan} and the original paper \cite{Socher}.
Our implementation performed only slightly better than randomly selecting a positive or negative sentiment. We then
read through the Matlab implementation of RAE and discovered that many details were left out of the texts. In 
hindsight we should have used the Matlab source code to inform our own implementation. This would have been the 
correct way to proceed because the project instructions were to replicate Socher's results. We've learned an important
lesson; when it comes to replicating results the ultimate ground truth is the source code.


\begin{thebibliography}{1}
    \bibitem{Socher} Socher, R., Pennington, J., Huang,E.H., Ng, A.Y. and Manning, C.D.  
    \emph{Semi-Supervised Recursive Autoencoders for Predicting Sentiment Distributions}. 
    2011.
    \bibitem{Elkan}  C. Elkan, “Learning the meanings of sentences,” 2014. [Online]. Available:
    http://cseweb.ucsd.edu/ elkan/250B/learningmeaning.pdf
    \bibitem{Altwaijry} H. Altwaijry, K. Lin, T. Yamada "CSE 250B Project Assignment 4" 2012 [Online]. Available:
        http://cseweb.ucsd.edu/users/elkan/250Bwinter2013/KuenHanRAEreport.pdf
\end{thebibliography}
\end{document}
