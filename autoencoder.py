import numpy as np


class Autoencoder:
    def __init__(self, W1, B1, W2, B2, V, c1, c2, n1, n2, t,
                 left_child, right_child, vector_length=20, alpha=0.2):
        self.VECTOR_LENGTH = vector_length
        self.W1 = W1
        self.B1 = B1
        self.W2 = W2
        self.B2 = B2
        self.V = V
        self.c1 = c1
        self.c2 = c2
        self.n1 = n1
        self.n2 = n2
        self.t = t
        self.left = left_child
        self.right = right_child
        self.isLeaf = False
        self.alpha = 0.2
        self.x = self.get_output()
        self.z1, self.z2 = self.get_reconstruction()
        self.p = self.get_predicted_label()
        self.e1 = self.get_reconstruct_error()

        if t.any():
            self.x_prime = 1.0 - self.x ** 2
            self.e2 = self.get_label_error()
            self.d_r = self.get_d_r()
            self.d_l = self.get_d_l()

    def get_N(self):
        return self.n1 + self.n2

    def get_node_error(self):
        return self.alpha * self.e1 + (1.0-self.alpha) * self.e2

    def get_total_error(self):
        return self.get_node_error() + self._get_error_recursive(self.left) + self._get_error_recursive(self.right)

    def _get_error_recursive(self, root):
        if root.isLeaf:
            return root.get_node_error()

        return root.get_node_error() + root._get_error_recursive(root.left) + root._get_error_recursive(root.right)

    def get_label_error(self):
        result = -1 * self.t.dot(np.log(self.p))

        return result

    def get_reconstruct_error(self):
        x1e = self.c1 - self.z1
        x2e = self.c2 - self.z2
        result = (self.n1 / (self.n1 + self.n2) * x1e.dot(x1e) +
                  self.n2 / (self.n1 + self.n2) * x2e.dot(x2e))

        return result

    def get_predicted_label(self):
        return self.softmax(self.V.dot(self.x))

    def get_reconstruction(self):
        result = np.tanh(self.W2.dot(self.x.reshape(20,1)) + self.B2)

        z1, z2 = (result[:self.VECTOR_LENGTH][:, 0],
                result[self.VECTOR_LENGTH:][:, 0])

        return z1, z2

    def get_output(self):
        result = np.tanh(self.W1.dot(np.concatenate((self.c1,
                                                    self.c2)).reshape(40, 1))
                         + self.B1)[:, 0]
        return result

    def tanh_prime(self, tanh_out):
        result = 1 - tanh_out ** 2
        return result

    def get_d_r(self):
        """ computes delta for the reconstruction output node """
        childs = self.n1 + self.n2
        c1 = self.n1 / childs * 2 * (self.c1 - self.z1)
        c2 = self.n2 / childs * 2 * (self.c2 - self.z2)
        intermediate = -1.0 * self.alpha * np.concatenate((c1, c2))
        prime = self.tanh_prime(np.concatenate((self.z1,
                                                 self.z2)))
        result = intermediate * prime

        return result

    def get_d_l(self):
        """ computes delta for the label output node """
        return (1.0 - self.alpha) * (self.p - self.t)

    def softmax(self, w):
        e = np.exp(w)
        dist = e / np.sum(e)

        return dist
