import cPickle as Pickle
import nltk

# create a matrix where each row is a tuple of (label, tokenized sentence)
with open("rt-polarity.neg") as f_neg, open("rt-polarity.pos") as f_pos:
    data = []
    for sentence in f_neg:
        tokens = nltk.word_tokenize(sentence)
        data.append([False, tokens])
    print 'False sentences: {0}'.format(len(data))
    for sentence in f_pos:
        tokens = nltk.word_tokenize(sentence)
        data.append([True, tokens])
    print 'False and Positive sentences: {0}'.format(len(data))

# create a dictionary that maps each word to an index into a word embedding
# matrix
vocab = set()
vocab_dict = {}

for example in data:
    for word in example[1]:
        vocab.add(word)

print 'Size of vocabulary = {0}'.format(len(vocab))

for idx, word in enumerate(vocab):
    print word
    vocab_dict[word] = idx

# test vocabulary dictionary
for example in data:
    for word in example[1]:
        vocab_dict[word]

Pickle.dump((vocab_dict, data), open("dataset.pickle", "wb"))
